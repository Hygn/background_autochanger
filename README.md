# Background Autochanger

Change background according to Wi-Fi connection status.

## OS support
* Windows

## Getting started
### Download
[Release](https://gitlab.com/Hygn/background_autochanger/-/releases)
### Example configuration file
All background image files should be in "backgrounds" folder.
``` JSON
{
    "default": "spring-carousel-image.jpg",
    "updatePeriod":1,
    "profiles":
    [
        {
            "SSID": "hygn5",
            "BSSID": "00:00:00:ff:ff:fe",
            "background": "interlude_25.png"
        },
        {
            "SSID": "hygn2",
            "BSSID": "00:00:00:ff:ff:ff",
            "background": "interlude_25.png"
        }
    ]
}
```
### Setting up default profile
When Wi-Fi is not connected or not defined in profiles this program automatically falls back to the default profile.
``` JSON
{
    "default": "spring-carousel-image.jpg",
    "updatePeriod":1,
    "profiles":[]
}
```
Value of key "default" is a relative path for default background.\
Value of key "updatePeriod" is an interval for checking Wi-Fi connection status.\
All keys are case-sensitive.
### Setting up profiles
``` JSON
{
    "default": "spring-carousel-image.jpg",
    "updatePeriod":1,
    "profiles":
    [
        profile data goes here
    ]
}
```
Profile data consists of three Keys "SSID","BSSID and "background".\
Key "SSID" and "BSSID" is SSID and BSSID of your Wi-Fi and "background" is relative path for background image.\
By executing "start_debug.cmd" Wi-Fi connection information can be found.
### Starting program
Executing provided "start.cmd" file will start a background process.
