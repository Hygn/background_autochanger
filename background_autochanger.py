from changeWallpaper import change
from fetchNetworkStatus import fetch
from plyer import notification
from pystray import MenuItem as item
from multiprocessing import Process, Queue, freeze_support
from PIL import Image
import pystray
import time
import json
import os
import sys
appName = 'Background Autochanger'


def sendNotification(title: str, description: str):
    try:
        notification.notify(title, description, app_name=appName, timeout=1)
    except:
        pass


def mainThread(mainQueue,runopt):
    p_wlanStatus = {}
    while True:
        if (not mainQueue.empty()) and str(mainQueue.get()) == 'stop':
            return False
        try:
            wlanStatus = fetch()
        except IndexError:
            wlanStatus = None
        except:
            sendNotification('Error', 'Cannot fetch WLAN Status')
            break
        if runopt == "debug":
            print(str(wlanStatus))
        if p_wlanStatus != wlanStatus:
            try:
                cfg = json.load(open(os.path.relpath('./config.json')))
            except:
                sendNotification('Startup Error', 'config.json Syntax Error')
                break
            if wlanStatus == None:
                change(cfg['default'])
                if runopt != None:
                    sendNotification('Change Background',
                                     f"Changing into {cfg['default']}")
            else:
                for i in cfg['profiles']:
                    if {'SSID': i['SSID'], 'BSSID': i['BSSID']} == wlanStatus:
                        change(i['background'])
                        if runopt != None:
                            sendNotification('Change Background',
                                             f"Changing into {i['background']}")
                        break
                    elif cfg['profiles'].index(i) + 1 == len(cfg['profiles']):
                        change(cfg['default'])
                        if runopt != None:
                            sendNotification('Change Background',
                                             f"Changing into {cfg['default']}")
        time.sleep(cfg['updatePeriod'])
        p_wlanStatus = wlanStatus


def terminateJob():
    icon.stop()
    mainJobQue.put('stop')


if __name__ == "__main__":
    freeze_support() # Required for pyinstaller
    try:
        if sys.argv[1] in ['debug', 'verbose']:
            runopt = sys.argv[1]
        else:
            runopt = None
    except IndexError:
        runopt = None

    mainJobQue = Queue()
    mainJobProcess = Process(target=mainThread, args=(mainJobQue,runopt))
    mainJobProcess.start()

    image = Image.open("mainicon.png")
    menu = (item('종료', terminateJob), )
    icon = pystray.Icon("name", image, appName, menu)
    icon.run()

    mainJobQue.close()
    mainJobQue.join_thread()
    mainJobProcess.join()
