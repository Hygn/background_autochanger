import ctypes
import os
from plyer import notification
def change(relpath:str):
   SPI_SETDESKWALLPAPER = 20 
   try:
      open(os.path.abspath(f"./backgrounds/{relpath}"))
      ctypes.windll.user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, os.path.abspath(f"./backgrounds/{relpath}") , 0)
   except:
      try:
         notification.notify("Background Change failed", "File not found", app_name="Background Autochanger", timeout=1)
      except:
         pass