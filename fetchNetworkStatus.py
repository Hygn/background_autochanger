import subprocess
import locale
def fetch():
    cp = locale.getdefaultlocale()
    results = subprocess.check_output(["netsh", "wlan", "show", "interfaces"], stdin=subprocess.DEVNULL)
    results = results.decode(cp[1])
    results = results.replace("\r","")
    ls = results.split("\n")
    wlanData = {}
    for i in ls:
        q = i.split(':',1)
        if "SSID" in q[0] and "BSSID" not in q[0]:
            wlanData.update({'SSID':q[1].replace(' ','',1)})
        if "BSSID" in q[0]:
            wlanData.update({'BSSID':q[1].replace(' ','',1)})
    return wlanData
